import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormGroupDirective, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home/home.component';
import { MaterialModule } from './material.module';
import { httpInterceptorProviders } from './http-inteceptors';
import { InputPageComponent } from './input-page/input-page.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { SampleComponent } from './sample/sample.component';
import { HighlightComponent } from './highlight/highlight.component';
import { ComparetableComponent } from './comparetable/comparetable.component';
import { SapTOq1Component } from './main/sap-toq1/sap-toq1.component';
import { Q1TOsapComponent } from './main/q1-tosap/q1-tosap.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    InputPageComponent,
    SampleComponent,
    HighlightComponent,
    ComparetableComponent,
    SapTOq1Component,
    Q1TOsapComponent,
    MainComponent,
    LoginComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule, MaterialModule, FormsModule, ReactiveFormsModule, HttpClientModule, ScrollingModule
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
