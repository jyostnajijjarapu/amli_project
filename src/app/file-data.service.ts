import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FileDataService {
  saptoq1data: any;
  q1tosapdata: any;
  sapmaterials = [];
  q1materials = [];
  sapExcelData: any;
  q1ExcelData: any;
  sapColNames = [];
  sapFile: any = '';
  q1File: any = '';
  // saptoq1datasummary: any;
  // saptoq1datapartial: any;
  // saptoq1dataexact: any;
  // saptoq1datanomatches: any;
  // saptoq1datastatistics: any;
  // q1tosapdatasummary: any;
  // q1tosapdatapartial: any;
  // q1tosapdataexact: any;
  // q1tosapdatanomatches: any;
  // q1tosapdatastatistics: any;
  constructor() { }
}
