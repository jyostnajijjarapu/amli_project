import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { menuItems } from '../sidemenu';
@Component({
  selector: 'app-comparetable',
  templateUrl: './comparetable.component.html',
  styleUrls: ['./comparetable.component.scss']
})
export class ComparetableComponent implements OnInit {
  data = [
    { 'sapIndex': 14, 'q1index': 394, 'sap': 11132262, 'Q1': '11132262_C_3577_C', 'type': '<= C10', 'partial': 'NO', 'exact': 'YES', 'primary': 'NO', 'saptruefalse': 'TRUE', 'upper': 1, 'lower': 0, 'ctq': 'TRUE', 'usl': 1, 'lsl': 0, 'test_method': '' },
    { 'sapIndex': 16, 'q1index': 395, 'sap': 11132262, 'Q1': '11132262_C_3577_C', 'type': '<= C14', 'partial': 'NO', 'exact': 'YES', 'primary': 'NO', 'saptruefalse': 'TRUE', 'upper': 1.5, 'lower': 0, 'ctq': 'TRUE', 'usl': 1.5, 'lsl': 0, 'test_method': '' },
    { 'sapIndex': 11, 'q1index': 396, 'sap': 11132262, 'Q1': '11132262_C_3577_C', 'type': 'Acid Value', 'partial': 'NO', 'exact': 'YES', 'primary': 'NO', 'saptruefalse': 'TRUE', 'upper': 282, 'lower': 278, 'ctq': 'TRUE', 'usl': 282, 'lsl': 278, 'test_method': '' },
    { 'sapIndex': 63, 'q1index': 51, 'sap': 83352033, 'Q1': '83352033_C_3577_C', 'type': 'Active Component', 'partial': 'YES', 'exact': 'NO', 'primary': 'NO', 'saptruefalse': 'FALSE', 'upper': 32, 'lower': 29, 'ctq': 'TRUE', 'usl': 32, 'lsl': 29, 'test_method': '' },
    { 'sapIndex': 63, 'q1index': 56, 'sap': 83352033, 'Q1': '83352033_C_3577_C', 'type': 'Active Component', 'partial': 'YES', 'exact': 'NO', 'primary': 'NO', 'saptruefalse': 'FALSE', 'upper': 32, 'lower': 29, 'ctq': 'TRUE', 'usl': 32, 'lsl': 29, 'test_method': '' },
    { 'sapIndex': 63, 'q1index': 62, 'sap': 83352033, 'Q1': '83352033_C_3577_C', 'type': 'Active Component', 'partial': 'YES', 'exact': 'NO', 'primary': 'NO', 'saptruefalse': 'FALSE', 'upper': 32, 'lower': 29, 'ctq': 'TRUE', 'usl': 32, 'lsl': 29, 'test_method': '' },


  ]
  menuOptions: any = JSON.parse(JSON.stringify(menuItems));

  constructor(public router: Router) { }

  ngOnInit(): void {
  }
  applyActiveLinkBasedOnRoute(menuItem: any) {
    return this.router.url.includes(menuItem.routerLink);
  }
}
