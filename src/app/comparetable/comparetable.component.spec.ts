import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComparetableComponent } from './comparetable.component';

describe('ComparetableComponent', () => {
  let component: ComparetableComponent;
  let fixture: ComponentFixture<ComparetableComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComparetableComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComparetableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
