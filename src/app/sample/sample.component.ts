import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpService } from '../http.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { FileDataService } from '../file-data.service';
import * as XLSX from 'xlsx';
import { Binary } from '@angular/compiler';

@Component({
  selector: 'app-sample',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.scss']
})
export class SampleComponent implements OnInit {
  filteredOptions: any;
  filteredOptions1: any;
  sapmaterials = [];
  q1materials = [];
  // sapmaterials: any = ['abc11', 'efg123', 'hyd435', 'huy4567', 'king345', 'lion'];
  // q1materials: any = ['abc11', 'efg123', 'hyd435', 'huy4567', 'king345', 'lion', "lllllll", "999999989", "877657fh"];
  sapFile: any = '';
  q1File: any = '';
  myControl = new FormControl();
  isLoader = false;
  count = 0;
  selectedFactory = '';
  sapselectedOption: any = 'saplist';
  q1selectedOption: any = 'q1list';

  isSelected = false;
  sapmaterialList: any;
  q1materialList: any;
  factories = ['Jaffersoncity'];
  selectedSAPMaterials: any = [];
  selectedQ1Materials: any = [];

  teamInitial = '';
  teamInitial1 = '';
  isSuccess = false;
  public pathname = '';
  sanitizedUrl: any;
  sapColumns = ['Material', 'Material Type', 'TRUE/FALSE', 'Upper limit', 'Lower limit'];
  selectedMaterial: any;
  selectedMaterialType: any;
  selectedtruefalse: any;
  selectedupperlimit: any;
  selectedlowerlimit: any;
  infofields = ['COA', 'Others']
  selectedInfoField: any;
  constructor(private httpService: HttpService, private snackBar: MatSnackBar, private sanitizer: DomSanitizer, private router: Router, public dataService: FileDataService) {

  }

  ngOnInit(): void {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
    this.filteredOptions1 = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter1(value))
      );
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.dataService.sapmaterials.filter((option: any) => option.toString().toLowerCase().includes(filterValue));
  }
  private _filter1(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.dataService.q1materials.filter((option: any) => option.toString().toLowerCase().includes(filterValue));
  }
  onSAPFileSelected(event: any) {
    this.sapFile = event.target.files[0];
    this.dataService.sapFile = event.target.files[0];
    let filereader = new FileReader();
    filereader.readAsBinaryString(this.sapFile)
    filereader.onload = (e) => {
      var wb = XLSX.read(filereader.result, { type: 'binary' })
      var ws = wb.SheetNames
      this.dataService.sapExcelData = XLSX.utils.sheet_to_json(wb.Sheets[ws[0]])
    }
  }
  onQ1FileSelected(event: any) {
    this.q1File = event.target.files[0];
    this.dataService.q1File = event.target.files[0];
    let filereader = new FileReader();
    filereader.readAsBinaryString(this.q1File)
    filereader.onload = (e) => {
      var wb = XLSX.read(filereader.result, { type: 'binary' })
      var ws = wb.SheetNames
      this.dataService.q1ExcelData = XLSX.utils.sheet_to_json(wb.Sheets[ws[0]])
    }
  }

  //upload files
  onUploadSAPQ1Files() {
    let body = new FormData();

    body.append('SAP data', this.sapFile)
    body.append('QA data', this.q1File)

    this.isLoader = true;
    this.httpService.post("upload", body).subscribe((res) => {
      // console.log(res, res.sap, res.Qone)
      if (res.sap_columns.length > 0) {
        this.dataService.sapmaterials = res.sap;
        this.dataService.q1materials = res.Qone;
        this.dataService.sapColNames = res.sap_columns;
        this.sapmaterials = res.sap;
        this.q1materials = res.Qone;
        // console.log(this.sapmaterials, this.q1materials)
        this.count = res.length;
        this.isLoader = false;
        this.snackBar.open("Files Uploaded Successfully!", " ", { 'duration': 2000, panelClass: 'blue-snackbar' });
        this.filteredOptions = this.myControl.valueChanges
          .pipe(
            startWith(''),
            map(value => this._filter(value))
          );
      }
      else {
        this.isLoader = false;
        this.snackBar.open("Something Went Wrong...", " ", { 'duration': 2000, panelClass: 'red-snackbar' });
      }

    }, err => {
      this.isLoader = false;
      this.snackBar.open("Something Went Wrong...", " ", { 'duration': 2000, panelClass: 'red-snackbar' });
    });
    // )
  }
  sapoptionSelection(event: any) {
    if (event.value != '') {
      this.isSelected = false;

    }
  }
  q1optionSelection(event: any) {
    if (event.value != '') {
      this.isSelected = false;

    }
  }
  sapmaterialSelection(event: any) {
    this.selectedSAPMaterials = event.value;
  }
  q1materialSelection(event: any) {
    this.selectedQ1Materials = event.value;
  }
  // compare() {
  //   var format = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|.<>\/?~]/;
  //   // console.log("hdlkvdbfjbbbbbbbbbbbbbbbbbbbbbz")

  //   if (this.sapselectedOption != '') {
  //     this.isSelected = false;
  //     let body = new FormData();

  //     if (this.sapselectedOption == 'saplist') {
  //       this.selectedSAPMaterials = [];
  //       let elements = this.sapmaterialList.replace(/[ `!@#$%^&*()_+\-=\[\]{};':"\\|.<>\/?~]/g, '')
  //       let list: any = [];
  //       list.push(elements);
  //       body.append('option', this.sapselectedOption)
  //       body.append('Materials SAP', list)

  //     }
  //     else {
  //       body.append('option', this.sapselectedOption)

  //       body.append('Materials SAP', this.selectedSAPMaterials)
  //       this.teamInitial = '';
  //       this.selectedSAPMaterials = [];

  //     }

  //     this.httpService.post("getdetails", body).subscribe(res => {
  //       console.log(res, "res")

  //     }, err => {

  //     })
  //   }
  //   else {
  //     this.isSelected = true;

  //   }
  // }
  compareSAPQ1() {
    // this.isLoader = true;
    if (this.sapselectedOption != '') {
      this.isSelected = false;
      let body = new FormData();
      let sapCols: any = { 'Material': this.selectedMaterial, 'Material Type': this.selectedMaterialType, 'TRUE/FALSE': this.selectedtruefalse, 'Upper limit': this.selectedupperlimit, 'Lower limit': this.selectedlowerlimit }

      if (this.sapselectedOption == 'saplist' || this.q1selectedOption == 'q1list') {
        this.selectedSAPMaterials = [];
        this.selectedQ1Materials = [];
        let sapelements = this.sapmaterialList.replace(/[ `!@#$%^&*()_+\-=\[\]{};':"\\|.<>\/?~]/g, '')
        let q1elements = this.q1materialList.replace(/[ `!@#$%^&*()_+\-=\[\]{};':"\\|.<>\/?~]/g, '')

        let saplist: any = [];
        let q1list: any = [];
        saplist.push(sapelements);
        q1list.push(q1elements)
        // body.append('option', this.sapselectedOption)
        body.append('Materials SAP', saplist)
        body.append('Materials QA', q1list)
        body.append('SAPColNames', JSON.stringify(sapCols))
        body.append('Info Field', this.selectedInfoField)
      }
      else {
        // body.append('option', this.sapselectedOption)

        body.append('Materials SAP', this.selectedSAPMaterials)
        body.append('Materials QA', this.selectedQ1Materials)
        body.append('SAPColNames', JSON.stringify(sapCols))
        body.append('Info Field', this.selectedInfoField)


        this.teamInitial = '';
        this.teamInitial1 = '';
        this.selectedSAPMaterials = [];
        this.selectedQ1Materials = [];

      }
      // this.router.navigate(['/main/saptoq1'])
      // const headers: any = new HttpHeaders({ 'Content-Type': 'multipart/form-data' });
      this.httpService.post("getdetails", body,).subscribe((response) => {

        console.log(response, "res")
        if (response) {
          this.dataService.saptoq1data = response
          this.router.navigate(['/main/saptoq1'])
          this.isSuccess = true;
          this.isLoader = false;

          //   // this.pathname = "ms-excel:ofe|u|file:///C:/Users/MadhuriJyostnaJijjar/Documents/jefforsoncity/" + response[1]

        }



      }, err => {
        this.isSuccess = false;

        this.isLoader = false;
        console.log(err, "error")
      })
      this.httpService.post('getdetails1', body).subscribe((res) => {
        if (res) {
          this.dataService.q1tosapdata = res
          console.log(res, "q1tosap")
        }
      })
    }
    else {
      this.isSelected = true;
      this.isLoader = false;
      this.isSuccess = false;

    }
    this.isSuccess = false;
  }

  sanitize(url: string) {
    return this.sanitizer.bypassSecurityTrustUrl(url);
  }
  // downloadFile(data: Blob) {
  //   const blob = new Blob([data], { type: '.xlsx' });
  //   const url = window.URL.createObjectURL(blob);
  //   window.open(url);
  // }
  download(data: any) {
    const blob = new Blob([data], { type: 'application/octet-stream' });
    const url = window.URL.createObjectURL(blob);
    window.open(url);
  }
  // downloadFile(data: Blob) {
  //   const contentType = 'application/vnd.openxmlformats-ficedocument.spreadsheetml.sheet';
  //   const blob = new Blob([data], { type: contentType });
  //   const url = window.URL.createObjectURL(blob);
  //   window.open(url);
  // }
  // downLoadFile(data: any, type: string) {
  //   let blob = new Blob([data], { type: type });
  //   let url = window.URL.createObjectURL(blob);
  //   let pwa = window.open(url);
  //   if (!pwa || pwa.closed || typeof pwa.closed == 'undefined') {
  //     alert('Please disable your Pop-up blocker and try again.');
  //   }
  // }

  onColumnSubmit() {
    let body = new FormData();
    let sapCols: any = { 'Material': this.selectedMaterial, 'Material Type': this.selectedMaterialType, 'TRUE/FALSE': this.selectedtruefalse, 'Upper limit': this.selectedupperlimit, 'Lower limit': this.selectedlowerlimit }
    body.append('SAPColNames', JSON.stringify(sapCols))
    this.httpService.post('rename', body).subscribe((res: any) => {
      if (res.sap.length > 0 && res.Qone.length > 0) {
        this.dataService.sapmaterials = res.sap;
        this.dataService.q1materials = res.Qone;
        this.dataService.sapColNames = res.sap_columns;
        this.sapmaterials = res.sap;
        this.q1materials = res.Qone;
      }
    })

  }
}

function JSONParse(response: any): ((value: any) => void) | Partial<import("rxjs").Observer<any>> | null | undefined {
  throw new Error('Function not implemented.');
}

