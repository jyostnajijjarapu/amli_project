import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Observable } from 'rxjs';
import { HttpService } from '../http.service';
import { map, startWith } from 'rxjs/operators';
@Component({
  selector: 'app-highlight',
  templateUrl: './highlight.component.html',
  styleUrls: ['./highlight.component.scss']
})
export class HighlightComponent implements OnInit {
  sapFile: any;
  q1File: any;
  selectedOption: any = 'list';
  selectedFactory: any;
  factories = ['Amli', 'Jaffersoncity', 'ABC'];
  materials: any = [];
  rangeFrom: any;
  rangeTo: any;
  selectedMaterials: any = [];
  isSelected = false;
  count: any = 100;
  isRangeSelected = false;
  teamInitial = '';
  isLoader = false;
  allSelected = false;
  isChecked = true;
  selected: any = [];
  public filteredList1 = this.materials.slice();

  selectedTexts: string[] = [];


  myForm: any;
  users: any;
  filteredUsers: any;
  myControl = new FormControl();
  filteredOptions!: Observable<string[]>;
  materialList: any;
  materialList1: any;
  newSet: any = "";
  constructor(private httpService: HttpService, private snackBar: MatSnackBar,) {


  }

  ngOnInit(): void {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.materials.filter((option: any) => option.toString().toLowerCase().includes(filterValue));
  }
  onSAPFileSelected(event: any) {
    this.sapFile = event.target.files[0]

  }
  onQ1FileSelected(event: any) {
    this.q1File = event.target.files[0]

  }
  materialSelection(event: any) {
    this.selectedMaterials = event.value;
  }
  onUploadSAPTOQ1() {
    let body = new FormData();

    body.append('SAP data', this.sapFile)
    body.append('QA data', this.q1File)

    this.isLoader = true;
    this.httpService.post("uploads2q", body).subscribe((res) => {

      if (res.length > 0) {

        this.materials = res;

        this.count = res.length;
        this.isLoader = false;
        this.snackBar.open("Files Uploaded Successfully!", " ", { 'duration': 2000, panelClass: 'blue-snackbar' });
        this.filteredOptions = this.myControl.valueChanges
          .pipe(
            startWith(''),
            map(value => this._filter(value))
          );
      }
      else {
        this.isLoader = false;
        this.snackBar.open("Something Went Wrong...", " ", { 'duration': 2000, panelClass: 'red-snackbar' });
      }

    }, err => {
      this.isLoader = false;
      this.snackBar.open("Something Went Wrong...", " ", { 'duration': 2000, panelClass: 'red-snackbar' });
    });
    // )
  }

  onUploadQ1TOSAP() {
    let body = new FormData();

    body.append('SAP data', this.sapFile)
    body.append('QA data', this.q1File)

    this.isLoader = true;
    this.httpService.post("uploadq2s", body).subscribe((res) => {

      if (res.length > 0) {

        this.materials = res;

        this.count = res.length;
        this.isLoader = false;
        this.snackBar.open("Files Uploaded Successfully!", " ", { 'duration': 2000, panelClass: 'blue-snackbar' });
        this.filteredOptions = this.myControl.valueChanges
          .pipe(
            startWith(''),
            map(value => this._filter(value))
          );
      }
      else {
        this.isLoader = false;
        this.snackBar.open("Something Went Wrong...", " ", { 'duration': 2000, panelClass: 'red-snackbar' });
      }

    }, err => {
      this.isLoader = false;
      this.snackBar.open("Something Went Wrong...", " ", { 'duration': 2000, panelClass: 'red-snackbar' });
    });
  }


  sapTOq1() {
    var format = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|.<>\/?~]/;
    console.log("hdlkvdbfjbbbbbbbbbbbbbbbbbbbbbz")

    if (this.selectedOption != '') {
      this.isSelected = false;
      let body = new FormData();

      if (this.selectedOption == 'list') {
        this.selectedMaterials = [];
        let elements = this.materialList.replace(/[ `!@#$%^&*()_+\-=\[\]{};':"\\|.<>\/?~]/g, '')
        let list: any = [];
        list.push(elements);
        body.append('option', this.selectedOption)
        body.append('Materials SAP', list)

      }
      else {
        body.append('option', this.selectedOption)

        body.append('Materials SAP', this.selectedMaterials)
        this.teamInitial = '';
        this.selectedMaterials = [];

      }

      this.httpService.post("getdetails", body).subscribe(res => {
        console.log(res, "res")

      }, err => {

      })
    }
    else {
      this.isSelected = true;

    }
  }
  q1TOsap() {

    if (this.selectedOption != '') {
      this.isSelected = false;
      let body = new FormData();

      if (this.selectedOption == 'list') {
        this.selectedMaterials = [];
        let elements = this.materialList1.replace(/[ `!@#$%^&*()_+\-=\[\]{};':"\\|.<>\/?~]/g, '')
        let list: any = [];
        list.push(elements);
        body.append('option', this.selectedOption)
        body.append('Materials SAP', list)

      }
      else {
        body.append('option', this.selectedOption)

        body.append('Materials SAP', this.selectedMaterials)
        this.teamInitial = '';
        this.selectedMaterials = [];

      }

      this.httpService.post("getdetails1", body).subscribe(res => {
        console.log(res, "res")

      }, err => {

      })
    }
    else {
      this.isSelected = true;

    }
  }
  download(data: any) {
    const blob = new Blob([data], { type: '.xlsx' });
    const url = window.URL.createObjectURL(blob);
    window.open(url);
  }

  optionSelection(event: any) {
    if (event.value != '') {
      this.isSelected = false;

    }
  }
}
