import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { menuItems } from '../../sidemenu';
import * as XLSX from 'xlsx';
import { FileDataService } from 'src/app/file-data.service';
import { HttpService } from 'src/app/http.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-sap-toq1',
  templateUrl: './sap-toq1.component.html',
  styleUrls: ['./sap-toq1.component.scss']
})
export class SapTOq1Component implements OnInit {
  @ViewChild('TABLE', { static: false }) TABLE!: ElementRef;
  @ViewChild('TABLE1', { static: false }) TABLE1!: ElementRef;
  @ViewChild('TABLE2', { static: false }) TABLE2!: ElementRef;

  @ViewChild('TABLE3', { static: false }) TABLE3!: ElementRef;

  @ViewChild('TABLE4', { static: false }) TABLE4!: ElementRef;

  data: any = [
    { 'sapIndex': 14, 'q1index': 394, 'sap': 11132262, 'Q1': '11132262_C_3577_C', 'type': '<= C10', 'partial': 'NO', 'exact': 'YES', 'primary': 'NO', 'saptruefalse': 'TRUE', 'upper': 1, 'lower': 0, 'ctq': 'TRUE', 'usl': 1, 'lsl': 0, 'test_method': '' },
    { 'sapIndex': 16, 'q1index': 395, 'sap': 11132262, 'Q1': '11132262_C_3577_C', 'type': '<= C14', 'partial': 'NO', 'exact': 'YES', 'primary': 'NO', 'saptruefalse': 'TRUE', 'upper': 1.5, 'lower': 0, 'ctq': 'TRUE', 'usl': 1.5, 'lsl': 0, 'test_method': '' },
    { 'sapIndex': 11, 'q1index': 396, 'sap': 11132262, 'Q1': '11132262_C_3577_C', 'type': 'Acid Value', 'partial': 'NO', 'exact': 'YES', 'primary': 'NO', 'saptruefalse': 'TRUE', 'upper': 282, 'lower': 278, 'ctq': 'TRUE', 'usl': 282, 'lsl': 278, 'test_method': '' },
    { 'sapIndex': 63, 'q1index': 51, 'sap': 83352033, 'Q1': '83352033_C_3577_C', 'type': 'Active Component', 'partial': 'YES', 'exact': 'NO', 'primary': 'NO', 'saptruefalse': 'FALSE', 'upper': 32, 'lower': 29, 'ctq': 'TRUE', 'usl': 32, 'lsl': 29, 'test_method': '' },
    { 'sapIndex': 63, 'q1index': 56, 'sap': 83352033, 'Q1': '83352033_C_3577_C', 'type': 'Active Component', 'partial': 'YES', 'exact': 'NO', 'primary': 'NO', 'saptruefalse': 'FALSE', 'upper': 32, 'lower': 29, 'ctq': 'TRUE', 'usl': 32, 'lsl': 29, 'test_method': '' },
    { 'sapIndex': 63, 'q1index': 62, 'sap': 83352033, 'Q1': '83352033_C_3577_C', 'type': 'Active Component', 'partial': 'YES', 'exact': 'NO', 'primary': 'NO', 'saptruefalse': 'FALSE', 'upper': 32, 'lower': 29, 'ctq': 'TRUE', 'usl': 32, 'lsl': 29, 'test_method': '' },


  ];
  summarydata: any = [];
  nomatchesdata: any = [];
  exactmatchesdata: any = [];
  partialmatchesdata: any = [];
  statisticsdata: any = [];
  arr: any = [
    [{ FirstColumn: "Some data", SecondColumn: "Page 1 with 1 data" }],
    [
      { FirstColumn: "Some data-1", SecondColumn: "Page 2 with 2 data-1" },
      { FirstColumn: "Some data-2", SecondColumn: "Page 2 with 2 data-2" }
    ],
    [
      { FirstColumn: "Some data-1", SecondColumn: "Page 3 with 3 data-1" },
      { FirstColumn: "Some data-2", SecondColumn: "Page 3 with 3 data-2" },
      { FirstColumn: "Some data-3", SecondColumn: "Page 3 with 3 data-3" }
    ],
    [
      { FirstColumn: "Some data-1", SecondColumn: "Page 3 with 3 data-1" },
      { FirstColumn: "Some data-2", SecondColumn: "Page 3 with 3 data-2" },
      { FirstColumn: "Some data-3", SecondColumn: "Page 3 with 3 data-3" }
    ],
    [
      { FirstColumn: "Some data-1", SecondColumn: "Page 3 with 3 data-1" },
      { FirstColumn: "Some data-2", SecondColumn: "Page 3 with 3 data-2" },
      { FirstColumn: "Some data-3", SecondColumn: "Page 3 with 3 data-3" }
    ]
  ];
  menuOptions: any = JSON.parse(JSON.stringify(menuItems));

  constructor(public router: Router, private dataService: FileDataService, private http: HttpService) { }

  ngOnInit(): void {
    this.summarydata = this.dataService.saptoq1data['summary'][0]
    this.nomatchesdata = this.dataService.saptoq1data['nomatches'][0]
    this.partialmatchesdata = this.dataService.saptoq1data['partialmatches'][0]
    this.exactmatchesdata = this.dataService.saptoq1data['exactmatches'][0]
    this.statisticsdata = this.dataService.saptoq1data['stats'][0]
  }
  applyActiveLinkBasedOnRoute(menuItem: any) {
    return this.router.url.includes(menuItem.routerLink);
  }
  download(): void {
    window.open(environment.apiUrl + 'download1');
    // let ele1: any = document.getElementById('summary-table')?.getElementsByTagName("td")
    // console.log(ele1, "eeeeeeeeee")
    // for (var i = 0; i < ele1.length; i++) {
    //   ele1[i].style.background = "green"
    // }
    // let ele2 = document.getElementById('nomatches-table')
    // console.log(ele2, "eeeeeeeeee")

    // let ele3 = document.getElementById('partial-table')
    // console.log(ele3, "eeeeeeeeee")

    // let ele4 = document.getElementById('exact-table')
    // console.log(ele4, "eeeeeeeeee")

    // let ele5 = document.getElementById('stat-table')
    // console.log(ele5, "eeeeeeeeee")

    // this.arr = ['summary-table', 'nomatches-table', 'partial-table', 'exact-table', 'stat-table']
    // let arra1 = [ele1, ele2, ele3, ele4, ele5]



    // this.arr = [this.dataService.sapExcelData, this.dataService.q1ExcelData, this.summarydata, this.nomatchesdata, this.partialmatchesdata, this.exactmatchesdata, this.statisticsdata]
    // const fileName = "saptoq1output.xlsx";
    // const sheetName = ["SAP File", "Q1 File", "Summary", "No matches", "Partial matches", "Exact matches", "Statistics"];

    // let wb = XLSX.utils.book_new();
    // let ws11 = XLSX.utils.json_to_sheet(this.dataService.sapExcelData)
    // let ws22 = XLSX.utils.json_to_sheet(this.dataService.q1ExcelData)
    // let ws1 = XLSX.utils.table_to_sheet(this.TABLE.nativeElement)
    // let ws2 = XLSX.utils.table_to_sheet(this.TABLE1.nativeElement)
    // let ws3 = XLSX.utils.table_to_sheet(this.TABLE2.nativeElement)
    // let ws4 = XLSX.utils.table_to_sheet(this.TABLE3.nativeElement)
    // let ws5 = XLSX.utils.table_to_sheet(this.TABLE4.nativeElement)
    // let arr = [ws11, ws22, ws1, ws2, ws3, ws4, ws5]
    // for (var i = 0; i < sheetName.length; i++) {
    //   // let ws = XLSX.utils.json_to_sheet(this.arr[i])
    //   // let ws = XLSX.utils.json_to_sheet(this.arr[i]);
    //   XLSX.utils.book_append_sheet(wb, arr[i], sheetName[i]);
    // }
    // XLSX.writeFile(wb, fileName);

  }
  // download() {
  //   window.open("https://insp2.azurewebsites.net/download1")
  //   // this.http.get("download1").subscribe((res: any) => {
  //   //   console.log(res)
  //   //   const contentType = 'application/vnd.openxmlformats-ficedocument.spreadsheetml.sheet';
  //   //   const blob = new Blob([res], { type: contentType });
  //   //   const url = window.URL.createObjectURL(blob);
  //   //   window.open(url);
  //   // })
  // }
  // download() {
  //   var style = "<style>.green { background-color: green; }</style>";
  //   var uri = 'data:application/vnd.ms-excel;base64,'
  //     , template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>'
  //     , base64 = function (s: any) { return window.btoa(unescape(encodeURIComponent(s))) }
  //     , format = function (s: any, c: any) { return s.replace(/{(\w+)}/g, function (m: any, p: any) { return c[p]; }) }
  //   return function (table: any, name: any) {
  //     if (!table.nodeType) table = document.getElementById(table)
  //     var ctx = { worksheet: name || 'Worksheet', table: table.innerHTML }
  //     window.location.href = uri + base64(format(template, ctx))
  //   }

  // }
  // download() {
  //   const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(
  //     this.TABLE.nativeElement
  //   );
  //   for (var i in ws) {
  //     console.log(ws[i]);
  //     if (typeof ws[i] != 'object') continue;
  //     let cell: any = XLSX.utils.decode_cell(i);

  //     ws[i].s = {
  //       // styling for all cells
  //       font: {
  //         name: 'arial',
  //       },
  //       alignment: {
  //         vertical: 'center',
  //         horizontal: 'center',
  //         wrapText: '1', // any truthy value here
  //       },
  //       border: {
  //         right: {
  //           style: 'thin',
  //           color: '000000',
  //         },
  //         left: {
  //           style: 'thin',
  //           color: '000000',
  //         },
  //       },
  //     };

  //     // if (cell.c == 6) {
  //     //   // first column
  //     //   ws[i].s.numFmt = 'DD-MM-YYYY'; // for dates
  //     //   ws[i].z = 'DD-MM-YYYY';
  //     // } else {
  //     //   ws[i].s.numFmt = '00'; // other numbers
  //     // }

  //     if (cell.r == 0) {
  //       // first row
  //       ws[i].s.border.bottom = {
  //         // bottom border
  //         style: 'thin',
  //         color: 'FF0000',
  //       };
  //     }

  //     if (cell.c == 13) {
  //       // every other row
  //       ws[i].s.fill = {
  //         // background color
  //         patternType: 'solid',
  //         fgColor: { rgb: 'b2b2b2' },
  //         bgColor: { rgb: '33FF33' },
  //       };
  //     }
  //   }
  //   const wb: XLSX.WorkBook = XLSX.utils.book_new();
  //   XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');
  //   XLSX.writeFile(wb, 'ScoreSheet.xlsx');
  // }
  // download() {
  //   this.arr = [this.summarydata, this.nomatchesdata, this.partialmatchesdata, this.exactmatchesdata, this.statisticsdata]
  //   const fileName = "q1tosapoutput.xlsx";
  //   const sheetName = ["Summary", "No matches", "Partial matches", "Exact matches", "Statistics"];

  //   let wb = XLSX.utils.book_new();
  //   for (var i = 0; i < sheetName.length; i++) {
  //     let ws = XLSX.utils.json_to_sheet(this.arr[i]);
  //     XLSX.utils.book_append_sheet(wb, ws, sheetName[i]);
  //   }
  //   XLSX.writeFile(wb, fileName);

  // }
  // download() {
  //   this.arr = [this.dataService.sapExcelData, this.dataService.q1ExcelData, this.summarydata, this.nomatchesdata, this.partialmatchesdata, this.exactmatchesdata, this.statisticsdata]
  //   const fileName = "saptoq1output.xlsx";
  //   const sheetName = ["SAP File", "Q1 File", "Summary", "No matches", "Partial matches", "Exact matches", "Statistics"];

  //   let wb = XLSX.utils.book_new();
  //   let ws11 = XLSX.utils.json_to_sheet(this.dataService.sapExcelData)
  //   let ws22 = XLSX.utils.json_to_sheet(this.dataService.q1ExcelData)
  //   let ws1 = XLSX.utils.table_to_sheet(this.TABLE.nativeElement)
  //   let ws2 = XLSX.utils.table_to_sheet(this.TABLE1.nativeElement)
  //   let ws3 = XLSX.utils.table_to_sheet(this.TABLE2.nativeElement)
  //   let ws4 = XLSX.utils.table_to_sheet(this.TABLE3.nativeElement)
  //   let ws5 = XLSX.utils.table_to_sheet(this.TABLE4.nativeElement)
  //   let arr = [ws11, ws22, ws1, ws2, ws3, ws4, ws5]
  //   for (var i = 0; i < sheetName.length; i++) {
  //     // let ws = XLSX.utils.json_to_sheet(this.arr[i])
  //     // let ws = XLSX.utils.json_to_sheet(this.arr[i]);
  //     XLSX.utils.book_append_sheet(wb, arr[i], sheetName[i]);
  //   }
  //   XLSX.writeFile(wb, fileName);
  // }
  backto() {
    this.router.navigate(['/filesComparision'])
  }
}
