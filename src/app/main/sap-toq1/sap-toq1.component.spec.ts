import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SapTOq1Component } from './sap-toq1.component';

describe('SapTOq1Component', () => {
  let component: SapTOq1Component;
  let fixture: ComponentFixture<SapTOq1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SapTOq1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SapTOq1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
