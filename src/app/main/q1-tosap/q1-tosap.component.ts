import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { menuItems } from '../../sidemenu';
import * as XLSX from 'xlsx';
import { FileDataService } from 'src/app/file-data.service';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-q1-tosap',
  templateUrl: './q1-tosap.component.html',
  styleUrls: ['./q1-tosap.component.scss']
})
export class Q1TOsapComponent implements OnInit {

  data = [
    { 'sapIndex': 14, 'q1index': 394, 'sap': 11132262, 'Q1': '11132262_C_3577_C', 'type': '<= C10', 'partial': 'NO', 'exact': 'YES', 'primary': 'NO', 'saptruefalse': 'TRUE', 'upper': 1, 'lower': 0, 'ctq': 'TRUE', 'usl': 1, 'lsl': 0, 'test_method': '' },
    { 'sapIndex': 16, 'q1index': 395, 'sap': 11132262, 'Q1': '11132262_C_3577_C', 'type': '<= C14', 'partial': 'NO', 'exact': 'YES', 'primary': 'NO', 'saptruefalse': 'TRUE', 'upper': 1.5, 'lower': 0, 'ctq': 'TRUE', 'usl': 1.5, 'lsl': 0, 'test_method': '' },
    { 'sapIndex': 11, 'q1index': 396, 'sap': 11132262, 'Q1': '11132262_C_3577_C', 'type': 'Acid Value', 'partial': 'NO', 'exact': 'YES', 'primary': 'NO', 'saptruefalse': 'TRUE', 'upper': 282, 'lower': 278, 'ctq': 'TRUE', 'usl': 282, 'lsl': 278, 'test_method': '' },
    { 'sapIndex': 63, 'q1index': 51, 'sap': 83352033, 'Q1': '83352033_C_3577_C', 'type': 'Active Component', 'partial': 'YES', 'exact': 'NO', 'primary': 'NO', 'saptruefalse': 'FALSE', 'upper': 32, 'lower': 29, 'ctq': 'TRUE', 'usl': 32, 'lsl': 29, 'test_method': '' },
    { 'sapIndex': 63, 'q1index': 56, 'sap': 83352033, 'Q1': '83352033_C_3577_C', 'type': 'Active Component', 'partial': 'YES', 'exact': 'NO', 'primary': 'NO', 'saptruefalse': 'FALSE', 'upper': 32, 'lower': 29, 'ctq': 'TRUE', 'usl': 32, 'lsl': 29, 'test_method': '' },
    { 'sapIndex': 63, 'q1index': 62, 'sap': 83352033, 'Q1': '83352033_C_3577_C', 'type': 'Active Component', 'partial': 'YES', 'exact': 'NO', 'primary': 'NO', 'saptruefalse': 'FALSE', 'upper': 32, 'lower': 29, 'ctq': 'TRUE', 'usl': 32, 'lsl': 29, 'test_method': '' },


  ];
  summarydata: any = [];
  nomatchesdata: any = [];
  exactmatchesdata: any = [];
  partialmatchesdata: any = [];
  statisticsdata: any = [];
  arr = [
    [{ FirstColumn: "Some data", SecondColumn: "Page 1 with 1 data" }],
    [
      { FirstColumn: "Some data-1", SecondColumn: "Page 2 with 2 data-1" },
      { FirstColumn: "Some data-2", SecondColumn: "Page 2 with 2 data-2" }
    ],
    [
      { FirstColumn: "Some data-1", SecondColumn: "Page 3 with 3 data-1" },
      { FirstColumn: "Some data-2", SecondColumn: "Page 3 with 3 data-2" },
      { FirstColumn: "Some data-3", SecondColumn: "Page 3 with 3 data-3" }
    ],
    [
      { FirstColumn: "Some data-1", SecondColumn: "Page 3 with 3 data-1" },
      { FirstColumn: "Some data-2", SecondColumn: "Page 3 with 3 data-2" },
      { FirstColumn: "Some data-3", SecondColumn: "Page 3 with 3 data-3" }
    ],
    [
      { FirstColumn: "Some data-1", SecondColumn: "Page 3 with 3 data-1" },
      { FirstColumn: "Some data-2", SecondColumn: "Page 3 with 3 data-2" },
      { FirstColumn: "Some data-3", SecondColumn: "Page 3 with 3 data-3" }
    ]
  ];
  menuOptions: any = JSON.parse(JSON.stringify(menuItems));

  constructor(public router: Router, private dataService: FileDataService) { }

  ngOnInit(): void {
    this.summarydata = this.dataService.q1tosapdata['summary'][0]
    this.nomatchesdata = this.dataService.q1tosapdata['nomatches'][0]
    this.partialmatchesdata = this.dataService.q1tosapdata['partialmatches'][0]
    this.exactmatchesdata = this.dataService.q1tosapdata['exactmatches'][0]
    this.statisticsdata = this.dataService.q1tosapdata['stats'][0]
  }
  applyActiveLinkBasedOnRoute(menuItem: any) {
    return this.router.url.includes(menuItem.routerLink);
  }
  download() {
    window.open(environment.apiUrl + 'download2')
    // this.arr = [this.summarydata, this.nomatchesdata, this.partialmatchesdata, this.exactmatchesdata, this.statisticsdata]
    // const fileName = "q1tosapoutput.xlsx";
    // const sheetName = ["Summary", "No matches", "Partial matches", "Exact matches", "Statistics"];

    // let wb = XLSX.utils.book_new();
    // for (var i = 0; i < sheetName.length; i++) {
    //   let ws = XLSX.utils.json_to_sheet(this.arr[i]);
    //   XLSX.utils.book_append_sheet(wb, ws, sheetName[i]);
    // }
    // XLSX.writeFile(wb, fileName);

  }
  backto() {
    this.router.navigate(['/filesComparision'])
  }
}
