import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Q1TOsapComponent } from './q1-tosap.component';

describe('Q1TOsapComponent', () => {
  let component: Q1TOsapComponent;
  let fixture: ComponentFixture<Q1TOsapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Q1TOsapComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Q1TOsapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
