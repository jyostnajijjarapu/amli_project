import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { menuItems } from '../sidemenu';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  menuOptions: any = JSON.parse(JSON.stringify(menuItems));

  constructor(public router: Router) { }

  ngOnInit(): void {
  }
  applyActiveLinkBasedOnRoute(menuItem: any) {
    return this.router.url.includes(menuItem.routerLink);
  }
  logout() {
    this.router.navigate(['/login']);
  }
}
