import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main.component';
import { Q1TOsapComponent } from './q1-tosap/q1-tosap.component';
import { SapTOq1Component } from './sap-toq1/sap-toq1.component';

const routes: Routes = [
  {
    path: '', component: MainComponent,
    children: [

      { path: 'saptoq1', component: SapTOq1Component },
      { path: 'q1tosap', component: Q1TOsapComponent },

    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
