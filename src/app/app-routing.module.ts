import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComparetableComponent } from './comparetable/comparetable.component';
import { HighlightComponent } from './highlight/highlight.component';
import { HomeComponent } from './home/home.component';
import { InputPageComponent } from './input-page/input-page.component';
import { Q1TOsapComponent } from './main/q1-tosap/q1-tosap.component';
import { SampleComponent } from './sample/sample.component';
import { SapTOq1Component } from './main/sap-toq1/sap-toq1.component';
import { MainComponent } from './main/main.component';
import { LoginComponent } from './login/login.component';


const routes: Routes = [

  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  { path: 'filesComparision', component: SampleComponent },

  { path: 'home', component: HomeComponent },
  // { path: 'input', component: InputPageComponent },
  { path: 'input', component: SampleComponent },
  { path: 'compare', component: ComparetableComponent },
  // { path: 'main', component: MainComponent },
  {
    path: 'main', loadChildren: () => import('./main/main.module').then(m => m.MainModule)
  }
  // { path: 'q1tosap', component: Q1TOsapComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
