import { MatSelect } from '@angular/material/select';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpService } from '../http.service';
import * as XLSX from 'xlsx';

import {
  Component,
  ViewChild,
  ViewChildren,
  QueryList,
  ChangeDetectorRef
} from "@angular/core";
import { FormBuilder, FormControl } from "@angular/forms";
import {
  CdkVirtualScrollViewport,
  ScrollDispatcher
} from "@angular/cdk/scrolling";
import { MatOption } from "@angular/material/core";
import { filter } from "rxjs/operators";
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-input-page',
  templateUrl: './input-page.component.html',
  styleUrls: ['./input-page.component.scss']
})
export class InputPageComponent {
  sapFile: any;
  q1File: any;
  selectedOption: any = 'list';
  selectedFactory: any;
  factories = ['Amli', 'XYZ', 'ABC'];
  materials: any = [];
  // materials: any = ['Field_mounted_CF', 'Ball Value', 'Field mounted_L', 'Diaphragm', 'Normally Closed Ball Valve', 'Normally Closed Gate Valve', 'Gate Valve', 'Main control panel rear-mounted_SI', 'Main control panel front-mounted_SI', 'Globe Valve', 'Normally closed Valve', 'Field mounted_DI', 'Rectangle', 'Two value gate', 'Three gate Value', 'Main-Control panel rear mounted_CF', 'MainControlfrontpanel_DI',
  //   'Reducers', 'malicious', 'pump'];
  // isMaterialChecked = false;
  // isChecked = false;
  rangeFrom: any;
  rangeTo: any;
  selectedMaterials: any = [];
  isSelected = false;
  count: any = 100;
  isRangeSelected = false;
  teamInitial = '';
  isLoader = false;
  allSelected = false;
  isChecked = true;
  selected: any = [];
  public filteredList1 = this.materials.slice();
  @ViewChild('select')
  select!: MatSelect;
  @ViewChildren(MatOption)
  options!: QueryList<MatOption>;
  selectedTexts: string[] = [];
  @ViewChild(CdkVirtualScrollViewport, { static: true })
  cdkVirtualScrollViewPort!: CdkVirtualScrollViewport;
  multiSelectControl = new FormControl();
  myForm: any;
  users: any;
  filteredUsers: any;
  myControl = new FormControl();
  filteredOptions!: Observable<string[]>;
  materialList: any;
  newSet: any = "";
  constructor(private httpService: HttpService, private snackBar: MatSnackBar, private cd: ChangeDetectorRef, readonly sd: ScrollDispatcher, private fb: FormBuilder) {


  }

  getmaterials() {

  }

  ngOnInit(): void {
    this.filteredOptions = this.myControl.valueChanges
      .pipe(
        startWith(''),
        map(value => this._filter(value))
      );
  }
  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.materials.filter((option: any) => option.toString().toLowerCase().includes(filterValue));
  }
  onSAPFileSelected(event: any) {
    this.sapFile = event.target.files[0]

  }
  onQ1FileSelected(event: any) {
    this.q1File = event.target.files[0]

  }
  materialSelection(event: any) {
    this.selectedMaterials = event.value;
  }
  onUpload() {
    let body = new FormData();

    body.append('SAP data', this.sapFile)
    body.append('QA data', this.q1File)
    // body.append('factory', this.selectedFactory)
    this.isLoader = true;
    this.httpService.post("upload", body).subscribe((res) => {

      if (res.length > 0) {

        this.materials = res;
        // console.log(res)
        this.count = res.length;
        this.isLoader = false;
        this.snackBar.open("Files Uploaded Successfully!", " ", { 'duration': 2000, panelClass: 'blue-snackbar' });
        this.filteredOptions = this.myControl.valueChanges
          .pipe(
            startWith(''),
            map(value => this._filter(value))
          );
      }
      else {
        this.isLoader = false;
        this.snackBar.open("Something Went Wrong...", " ", { 'duration': 2000, panelClass: 'red-snackbar' });
      }

    }, err => {
      this.isLoader = false;
      this.snackBar.open("Something Went Wrong...", " ", { 'duration': 2000, panelClass: 'red-snackbar' });
    });
    // )
  }




  sapTOq1() {
    // let input: any = document
    //   .getElementById('listOfMaterials');

    // var textareas: any = document.getElementsByTagName("textarea");


    // var street_1: any = document.getElementById('listOfMaterials');
    // input.style.color = "red";

    // var element: any = document.getElementById("listOfMaterials");

    // var htmlText = element.innerHTML;

    // var i = htmlText.indexOf("(");
    // var j = htmlText.indexOf(")") + 1;

    // var redText = htmlText.substring(i, j);

    // element.innerHTML = htmlText.substring(0, i) + "<span style='color:red;'>" + redText + "</span>" + htmlText.substring(j);

    // var innerHTML = input.innerHTML;
    // var i, l;

    // input.focus();
    // input.value = input.value.trim();

    // i = input.value.indexOf('@');
    // l = ('@').length;

    // // https://developer.mozilla.org/en-US/docs/Web/API/HTMLInputElement/setSelectionRange

    // input
    //   .setSelectionRange(i, l + i);
    // input.innerHTML = input.value.replace('o', '<span style="color: red;">o</span>');

    // var str = "aadsa&ss))as(d?";
    // var cpy = input.value;
    // var indexes = [];
    // cpy = cpy.replace(/,/g, 'i');
    // while (/\W/.test(cpy)) {
    //   indexes.push(cpy.search(/\W/));
    //   cpy = cpy.replace(/\W/, 'i');
    // }
    // if (indexes.length > 0) {
    //   // for (var index in textareas) {

    //   console.log(textareas[index], "lllllll")
    //   console.log(index, "index")
    //   textareas[3].style.color = "red";
    // }
    // for (let i = 0; i < indexes.length; i++) {


    // input.innerHTML = input.innerHTML.replace(input.value[indexes[i]], '<span style="color: red;">input.value[indexes[i]]</span>');

    // var l = this.materialList[indexes[i]].length
    // this.newSet += input.value.charAt([indexes[i]]).fontcolor('red')
    // input.innerHTML = this.newSet

    // var index: any = this.materialList[indexes[i]]
    // for (index in textareas) {
    //   console.log(index, "kkkkkk")
    //   // if (index == indexes[i]) {
    //   //   textareas[index].style.color = 'yellow'
    //   // }
    //   // else {
    //   //   textareas[index].style.color = 'black'

    //   // }
    //   // console.log(input.value)

    //   // innerHTML = input.value[indexes[i]].style.color = "red";
    // }

    // console.log(input.value[indexes[i]])
    // console.log(innerHTML)
    // innerHTML = innerHTML.substring(0, indexes[i]) + "<span class='highlight'>" + innerHTML.substring(indexes[i], indexes[i] + input.value[indexes[i]].length) + "</span>" + innerHTML.substring(indexes[i] + input.value[indexes[i]].length);
    // input.innerHTML = innerHTML;
    // input.value[indexes[i]].style.color = "red"
    // }
    // }
    // console.log(indexes, "iiiiiiiii");


    var format = /[ `!@#$%^&*()_+\-=\[\]{};':"\\|.<>\/?~]/;

    let elements = this.materialList.replace(/[ `!@#$%^&*()_+\-=\[\]{};':"\\|.<>\/?~]/g, '')
    let list: any = [];
    list.push(elements);
    if (this.selectedOption != '') {
      this.isSelected = false;
      let body = new FormData();
      // this.isLoader = true;/
      if (this.selectedOption == 'list') {
        body.append('option', this.selectedOption)
        body.append('Materials SAP', list)

      }
      else {
        body.append('option', this.selectedOption)
        // console.log(this.selectedMaterials, "selectedmaterials")
        body.append('Materials SAP', this.selectedMaterials)
        this.teamInitial = '';
        this.selectedMaterials = [];

      }

      this.httpService.post("getdetails", body).subscribe(res => {
        console.log(res, "res")
        // if (res == 'materials are not found') {
        //   this.snackBar.open("materials are not found..Try again", " ", { 'duration': 2000, verticalPosition: 'top', panelClass: 'red-snackbar' });


        // }
      }, err => {
        // this.isLoader = false;
        // this.snackBar.open("Something Went Wrong...", " ", { 'duration': 2000, verticalPosition: 'top', panelClass: 'red-snackbar' });
      })
    }
    else {
      this.isSelected = true;

    }
  }
  download(data: any) {
    const blob = new Blob([data], { type: '.xlsx' });
    const url = window.URL.createObjectURL(blob);
    window.open(url);
  }

  optionSelection(event: any) {
    if (event.value != '') {
      this.isSelected = false;

    }
  }

}
