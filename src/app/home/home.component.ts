import { HttpClient } from '@angular/common/http';
import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { HttpService } from '../http.service';
// interface PeriodicElement {
//   name: string;
//   // position: number;
//   fill: number;
//   // symbol: string;
// }
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {
  factories = ['Amli', 'XYZ', 'ABC'];

  fillQ1Val: any;
  displayedColumns: string[] = ['name', 'fill', 'action'];
  displayedColumns1: string[] = ['colname', 'fillval', 'delAction'];
  displayedColumns2: string[] = ['sapcolname', 'q1colname', 'delAction'];
  displayedColumns3: string[] = ['sapcolname', 'sapcolvalue', 'q1colname', 'q1colvalue', 'delAction'];

  dataSource = new MatTableDataSource();
  dataSource1 = new MatTableDataSource();
  dataSource2 = new MatTableDataSource();
  dataSource3 = new MatTableDataSource();

  myColumns = {
    'sap': ['Unnamed: 0', 'Material', 'Plant', 'Task List Type',

      'Deletion Indicator', 'Task list number', 'Task list description',

      'Group Counter', 'Task list usage', 'Number of the task list node',

      'Internal counter', 'Status', 'Inspection Point', 'Vendor',

      'Dynamic Modification Rule', 'Dynamic Modification Level',

      'Activity Number', 'Workcenter', 'Dynamic Modification Level.1',

      'Control key', 'Operation short text',

      'Inspection Characteristic Number', 'Master Inspection Characteristics',

      'Plant for the Master Inspection Characte', 'Version Number',

      'Short Text for Inspection Characteristi', 'Sampling Procedure',

      'Text Line', 'Lower Tolerance Limit', 'Upper Specification Limit',

      'Target Value', 'Unit of Measurement', 'Quant or Qual charac',

      'Record measured Values', 'Characteristic Attributes', 'Upper Spec',

      'Lower Spec', 'Target Value.1', 'Scope', 'Long term inspection',

      'Recording type', 'Documentation', 'Documentation.1', 'Documentation.2',

      'Documentation.3', 'Documentation.4', 'Charac. req. or not',

      'Synchronization is Active', 'Additive charac.', 'Destructive charac.',

      'Formula', 'Sampling proc. Required', 'Scrap Share/Q score', 'Fixed',

      'Record # defects', 'Subsystem'],



    'q1': ['Unnamed: 0', 'id', 'Unnamed: 1', 'object_type__v.name__v',

      'object_type__v.api_name__v', 'global_id__sys', 'link__sys', 'name__v',

      'status__v', 'created_by__v', 'created_date__v', 'modified_by__v',

      'modified_date__v', 'characteristic__v',

      'characteristic__v.global_id__sys', 'characteristic__v.name__v',

      'characteristic__v.external_id__c', 'ctq__v', 'inspection_plan__v',

      'inspection_plan__v.global_id__sys', 'inspection_plan__v.name__v',

      'inspection_plan__v.external_id__c', 'lsl__v',

      'specification_source__v', 'units__v', 'units__v.global_id__sys',

      'units__v.name__v', 'units__v.external_id__c', 'usl__v', 'lifecycle__v',

      'state__v', 'state_stage_id__sys', 'state_stage_id__sys.name__v',

      'state_stage_id__sys.global_id__sys', 'stage__sys',

      'stage__sys.name__v', 'stage__sys.global_id__sys', 'coc_available__c',

      'execution_rule__c', 'execution_rule__c.global_id__sys',

      'execution_rule__c.name__v', 'inspection_method__c',

      'inspection_method__c.global_id__sys', 'inspection_method__c.name__v',

      'inspection_method__c.external_id__c', 'long_term__c',

      'requirement_use__c', 'target_value__c', 'test_method__c',

      'test_method__c.global_id__sys', 'test_method__c.name__v',

      'test_method__c.external_id__c', 'dutch_characteristic__c',

      'portuguese_characteristic__c', 'item__c', 'item__c.name__v',

      'item__c.external_id__v', 'item__c.global_id__sys',

      'item__c.sap_code__c', 'item__c.title__v', 'itemv__c',

      'itemv__c.global_id__sys', 'itemv__c.name__v',

      'itemv__c.external_id__c', 'result_type__c', 'spec_parameter__c',

      'spec_parameter__c.global_id__sys', 'spec_parameter__c.name__v',

      'spec_parameter__c.external_id__c', 'spec_property_type__c',

      'inspection_plan_type__c', 'sourcing_unit__c',

      'sourcing_unit__c.name__v', 'sourcing_unit__c.number__v',

      'sourcing_unit__c.external_id__v', 'sourcing_unit__c.global_id__sys',

      'condition_set__c', 'condition_set__c.global_id__sys',

      'condition_set__c.name__v', 'condition_set__c.external_id__c',

      'property_type__c', 'range_uom__c', 'range_uom__c.global_id__sys',

      'range_uom__c.name__v', 'range_uom__c.external_id__c', 'max_range__c',

      'min_range__c', 'reference_notes__c'],

  };
  sapColumnValues: any = {
    'Sampling Procedure': ['SPGEN02', 'SPGEN01'],
    'Text Line': ['Req/Cr', NaN, 'micro', 'COA', 'Cr/COA'],
    'Lower Tolerance Limit': [0],
    'Upper Specification Limit': [100, 0, 2, 10, 200],
    'Target Value': [0],
    'Unit of Measurement': ['CFG', 'MGK', NaN],
    'Quant or Qual charac': ['X', NaN],
    'Record measured Values': ['X', NaN],
    'Characteristic Attributes': [NaN, 'X'],
    'Upper Spec': ['X', NaN],
    'Lower Spec': [NaN, 'X'],
    'Target Value.1': [NaN],
    'Scope': ['='],
    'Long term inspection': [NaN],
    'Recording type': [NaN],
    'Documentation': ['.'],
    'Documentation.1': ['X', NaN],
    'Documentation.2': [NaN],
    'Documentation.3': [NaN],
    'Documentation.4': [NaN],
    'Charac. req. or not': [NaN],
    'Synchronization is Active': ['X'],
    'Additive charac.': [NaN],
    'Destructive charac.': [NaN],
    'Formula': [NaN],
    'Sampling proc. Required': [NaN],
    'Scrap Share/Q score': [NaN],
    'Fixed': [NaN],
    'Record # defects': [NaN],
    'Subsystem': ['X', NaN]
  };
  q1ColumnValues: any = {
    'Sampling Procedure': ['SPGEN02', 'SPGEN01'],
    'Text Line': ['Req/Cr', NaN, 'micro', 'COA', 'Cr/COA'],
    'Lower Tolerance Limit': [0],
    'Upper Specification Limit': [100, 0, 2, 10, 200],
    'Target Value': [0],
    'Unit of Measurement': ['CFG', 'MGK', NaN],
    'Quant or Qual charac': ['X', NaN],
    'Record measured Values': ['X', NaN],
    'Characteristic Attributes': [NaN, 'X'],
    'Upper Spec': ['X', NaN],
    'Lower Spec': [NaN, 'X'],
    'Target Value.1': [NaN],
    'Scope': ['='],
    'Long term inspection': [NaN],
    'Recording type': [NaN],
    'Documentation': ['.'],
    'Documentation.1': ['X', NaN],
    'Documentation.2': [NaN],
    'Documentation.3': [NaN],
    'Documentation.4': [NaN],
    'Charac. req. or not': [NaN],
    'Synchronization is Active': ['X'],
    'Additive charac.': [NaN],
    'Destructive charac.': [NaN],
    'Formula': [NaN],
    'Sampling proc. Required': [NaN],
    'Scrap Share/Q score': [NaN],
    'Fixed': [NaN],
    'Record # defects': [NaN],
    'Subsystem': ['X', NaN]
  }
  sapCols: any = [];
  q1Cols: any = [];
  sapColValues: any = [];
  sapColName: any;
  fillSAPVal: any;
  saparr: any = [];
  q1arr: any = [];
  sapcolvals: any = [];
  q1colvals: any = [];
  matchingSAPCol: any;
  matchingQ1Col: any;
  q1ColName: any;
  matchingSAPCols: any = [];
  matchingQ1Cols: any = [];
  matchingColsArr: any = [];
  q1ColValues: any;
  mappingSapColName: any;
  mappingQ1ColName: any;
  mappingSapColVal: any;
  mappingQ1ColVal: any;
  mappingColArr: any = [];
  isSAPAddFillNA = false;
  isQ1AddFillNA = false;
  isAddMatchingCols = false;
  isAddMappingCols = false;
  mappingSAPcolValues: any = [];
  mappingQ1colValues: any = [];

  constructor(private http: HttpService) { }

  ngOnInit(): void {
    this.sapCols = Object.keys(this.sapColumnValues);
    this.q1Cols = Object.keys(this.q1ColumnValues);
  }

  onSAPColSelection(event: any) {
    console.log(event.value, "vvvvvvvv")
    let selectedVal = event.value;
    this.sapColValues = this.sapColumnValues[selectedVal]
  }

  onFactorySelection(event: any) {
    let selectedVal = event.value;
    this.q1ColValues = this.q1ColumnValues[selectedVal]
  }

  deleteRowData(ele: any) {
    this.saparr = this.saparr.filter((i: any) => i !== ele)
    this.dataSource.data = this.dataSource.data.filter(i => i !== ele)
    let arr = [];
    for (let i = 0; i < this.saparr.length; i++) {
      arr.push(this.saparr[i].name)
    }
    this.sapcolvals = arr;
  }

  deleteRowData1(ele: any) {
    this.q1arr = this.q1arr.filter((i: any) => i !== ele)
    this.dataSource1.data = this.dataSource1.data.filter(i => i !== ele);
    let arr = [];
    for (let i = 0; i < this.q1arr.length; i++) {
      arr.push(this.q1arr[i].name)
    }
    this.q1colvals = arr;
  }

  deleteRowData2(ele: any) {
    this.matchingColsArr = this.matchingColsArr.filter((i: any) => i !== ele)
    this.dataSource2.data = this.dataSource2.data.filter(i => i !== ele);
    let arr1 = [];
    let arr2 = [];
    for (let i = 0; i < this.matchingColsArr.length; i++) {
      arr1.push(this.matchingColsArr[i].sapColName)
      arr2.push(this.matchingColsArr[i].q1ColName)
    }
    this.matchingSAPCols = arr1;
    this.matchingQ1Cols = arr2;
  }

  deleteRowData3(ele: any) {
    this.mappingColArr = this.mappingColArr.filter((i: any) => i !== ele)
    this.dataSource3.data = this.dataSource3.data.filter(i => i !== ele)
    let arr1 = [];
    let arr2 = [];
    for (let i = 0; i < this.mappingColArr.length; i++) {
      arr1.push(this.mappingColArr[i].sapColVal)
      arr2.push(this.mappingColArr[i].q1colvalue)
    }
    this.mappingSAPcolValues = arr1;
    this.mappingQ1colValues = arr2;
  }

  addSapFillNA() {
    this.isSAPAddFillNA = true;
    this.sapcolvals.push(this.sapColName);
    let obj = { 'name': String, 'fill': String };
    obj.name = this.sapColName;
    obj.fill = this.fillSAPVal;
    this.saparr.push(obj);
    this.dataSource.data = this.saparr;
    this.sapColName = '';
    this.fillSAPVal = '';
  }

  addQ1FillNA() {
    this.isQ1AddFillNA = true;
    this.q1colvals.push(this.q1ColName)
    let obj = { 'name': String, 'fill': String };
    obj.name = this.q1ColName;
    obj.fill = this.fillQ1Val;
    this.q1arr.push(obj);
    this.dataSource1.data = this.q1arr;
    this.q1ColName = '';
    this.fillQ1Val = '';
  }

  addMatchingCols() {
    this.isAddMatchingCols = true;
    this.matchingSAPCols.push(this.matchingSAPCol);
    this.matchingQ1Cols.push(this.matchingQ1Col);
    let matchingobj = { 'sapColName': String, 'q1ColName': String };
    matchingobj.sapColName = this.matchingSAPCol;
    matchingobj.q1ColName = this.matchingQ1Col;
    this.matchingColsArr.push(matchingobj);
    this.dataSource2.data = this.matchingColsArr;
    this.matchingSAPCol = '';
    this.matchingQ1Col = '';
  }

  addColumnMappingValues() {
    this.isAddMappingCols = true;
    let obj = { 'sapColName': String, 'sapColVal': String, 'q1colname': String, 'q1colvalue': String };
    obj.sapColName = this.mappingSapColName;
    obj.sapColVal = this.mappingSapColVal;
    obj.q1colname = this.mappingQ1ColName;
    obj.q1colvalue = this.mappingQ1ColVal;
    this.mappingColArr.push(obj);
    this.mappingSAPcolValues.push(this.mappingSapColVal);
    this.mappingQ1colValues.push(this.mappingQ1ColVal);

    this.dataSource3.data = this.mappingColArr;
    this.mappingSapColName = ''; this.mappingSapColVal = '';
    this.mappingQ1ColName = ''; this.mappingQ1ColVal = '';
  }

  onSubmit() {
    let body = new FormData();
    body.append('sapfillvalues', JSON.stringify(this.saparr))
    body.append('q1fillvalues', JSON.stringify(this.q1arr))
    this.http.post("fillNAValues", body).subscribe((res: any) => {
      console.log("res")
    })
  }

  isDisable(item: any) {
    var index = this.sapcolvals.indexOf(item);
    if (index == -1) {
      return false;
    }
    else {
      return true;
    }
  }

  isQ1Disable(item: any) {
    var index = this.q1colvals.indexOf(item);
    if (index == -1) {
      return false;
    }
    else {
      return true;
    }
  }

  isMatchSAPDisable(item: any) {
    var index = this.matchingSAPCols.indexOf(item);
    if (index == -1) {
      return false;
    }
    else {
      return true;
    }
  }

  isMatchQ1Disable(item: any) {
    var index = this.matchingQ1Cols.indexOf(item);
    if (index == -1) {
      return false;
    }
    else {
      return true;
    }
  }
  isMappingSAPDisable(item: any) {
    console.log(this.sapColValues, "sapcols")
    var index = this.mappingSAPcolValues.indexOf(item);
    if (index == -1) {
      return false;
    }
    else {
      return true;
    }
  }
  isMappingQ1Disable(item: any) {
    console.log(this.q1ColValues, "q1cols")
    var index = this.mappingQ1colValues.indexOf(item);
    if (index == -1) {
      return false;
    }
    else {
      return true;
    }
  }
}
